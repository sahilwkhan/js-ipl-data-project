const csv = require('csv-parser')
const fs = require('fs')


function teamRecordWonWithToss(matchesData){

    let matchAndTossList;
   
    let teamList = matchesData.reduce((matchAndTossList, match) =>{
        if (matchAndTossList.hasOwnProperty(match.winner) && match.winner.length > 1 ){
            if (match.winner == match.toss_winner){
                matchAndTossList[match.winner]++;
            }
        }
        else{
            if (match.winner.length > 1 ){
                if (match.winner == match.toss_winner){
                    matchAndTossList[match.winner] = 1;
                }
                else{
                    matchAndTossList[match.winner] = 0;
                }
            }
        }
        return matchAndTossList;
    },{});

    return teamList;

}



function teamsThatWonTossAndMatch(){

    const matchesFilePath = "src/data/matches.csv"

    const matchesData = [];

    // Parsing CSV data for utilizing raw data to get required statistics.

        fs.createReadStream(matchesFilePath)
        .pipe(csv())
        .on('data', (data) => matchesData.push(data))
        .on('end', () => {
            const output = teamRecordWonWithToss(matchesData);

            fs.writeFile("src/public/output/5-teams-that-won-toss-and-match.json",JSON.stringify(output), function (error){
                if (error){
                    console.log(error);
                }
                else{
                    console.log("File written successfully");
                    console.log("\nThe file writtened has the following data:");
                    console.log(fs.readFileSync("src/public/output/5-teams-that-won-toss-and-match.json", "utf8"));
                }
            })


        });

}

teamsThatWonTossAndMatch();