const csv = require('csv-parser')
const fs = require('fs')


function highestSuperOverEconomicBowler(deliveriesData){

    let bowlerRecord,delivery;
    let superOverDeliveries = deliveriesData.reduce((bowlerRecord,delivery) => {
        if (delivery.is_super_over == 1){
            if (bowlerRecord[delivery.bowler]){
                bowlerRecord[delivery.bowler].superOverRuns += parseInt(delivery.total_runs);
                bowlerRecord[delivery.bowler].balls++;
            }
            else{
                bowlerRecord[delivery.bowler] = {superOverRuns: parseInt(delivery.total_runs),balls: 1};
            }
        }
        return bowlerRecord;
    },{});

    // Store object with bowler and his economical rate in an array
    let superOverEconomicalRecord = [];
    let overs,overFractions,bowler;
    let superEconomicRate;
    for( let index in superOverDeliveries){
        bowler = superOverDeliveries[index];
        overFractions = (bowler.balls%6);
        overs = (overFractions == 0) ? (bowler.balls/6): (  ((bowler.balls - overFractions)/6)+ (overFractions/6)  ) ;
        superEconomicRate =  parseFloat( (bowler.superOverRuns/overs).toFixed(2) )
        superOverEconomicalRecord.push({ [index] :  superEconomicRate});
    }

    // Sort array of object having super over economical rate of bowlers in ascending order
    let firstBowler,secondBowler;
    superOverEconomicalRecord.sort( (firstBowler,secondBowler) =>{
        let rate1 = Object.values(secondBowler)[0]
        let rate2 = Object.values(firstBowler)[0]
        if (  rate1 > rate2){
            return -1;
        };   
    });

    // Return highest superover economical bowler
    return superOverEconomicalRecord[0];
}




function bestEconomicBowlerInSuperOvers(){

    const matchesFilePath = "src/data/matches.csv"
    const deliveriesFilePath = "src/data/deliveries.csv"

    const matchesData = [];
    const deliveriesData = [];

    // Parsing CSV data for utilizing raw data to get required statistics.
 
    fs.createReadStream(deliveriesFilePath)
    .pipe(csv())
    .on('data', (data) => deliveriesData.push(data))
    .on('end', () => {
        const output = highestSuperOverEconomicBowler(deliveriesData);

            fs.writeFile("src/public/output/9-bowler-with-best-economy-in-super-overs.json",JSON.stringify(output), function (error){
                if (error){
                    console.log(error);
                }
                else{
                    console.log("File written successfully");
                    console.log("\nThe file writtened has the following data:");
                    console.log(fs.readFileSync("src/public/output/9-bowler-with-best-economy-in-super-overs.json", "utf8"));
                }
            })

        });

}

bestEconomicBowlerInSuperOvers();