const csv = require('csv-parser')
const fs = require('fs');
const { totalmem } = require('os');

function highestDismissedPlayer(deliveriesData){
    let dismissedData, delivery;
    let batsman, bowler;
    let highestDismissal = {bowler : "", batsman : "", numberOfTimesDismissed : 0};

    let dismissedBatsmanList = deliveriesData.reduce((dismissedData,delivery) =>{
        
        if(delivery.player_dismissed){
            batsman = delivery.player_dismissed;
            bowler  = delivery.bowler; 
            
            if (dismissedData[bowler]){
                if (dismissedData[bowler][batsman]){
                    dismissedData[bowler][batsman]++;
                    if(dismissedData[bowler][batsman] >= highestDismissal.numberOfTimesDismissed){
                        highestDismissal.numberOfTimesDismissed = dismissedData[bowler][batsman];
                        highestDismissal.bowler = bowler;
                        highestDismissal.batsman = batsman;
                    }
                }
                else{
                    dismissedData[bowler][batsman] = 1;
                }
            }
            else{
                dismissedData[bowler] = {};
                dismissedData[bowler][batsman] = 1;
            }
        }
        return dismissedData;
    },{});

    return highestDismissal;
}





function strikeRateofAllBatsmanForAllYears(){

    const deliveriesFilePath = "src/data/deliveries.csv"
    const deliveriesData = [];

    // Parsing CSV data for utilizing raw data to get required statistics.

    fs.createReadStream(deliveriesFilePath)
    .pipe(csv())
    .on('data', (data) => deliveriesData.push(data))
    .on('end', () => {
        const output = highestDismissedPlayer(deliveriesData)
    
        fs.writeFile("src/public/output/8-highest-times-one-player-dismissed-another.json",JSON.stringify(output), function (error){
            if (error){
                console.log(error);
            }
            else{
                console.log("File written successfully");
                console.log("\nThe file writtened has the following data:");
                console.log(fs.readFileSync("src/public/output/8-highest-times-one-player-dismissed-another.json", "utf8"));
            }
        })

    });

}

strikeRateofAllBatsmanForAllYears();