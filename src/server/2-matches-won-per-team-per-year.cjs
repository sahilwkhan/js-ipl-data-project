const csv = require('csv-parser')
const fs = require('fs')



// returns object of matches that teams won per year in IPL

function matchesWonPerTeamEveryYear(matchesData){
    let winningStats = {};  
    let team, year;

    for (let match = 0; match <  matchesData.length; match++){
        if (matchesData[match].winner.length < 1){
           continue;
        }
        team = matchesData[match].winner;
        year = matchesData[match].season;

        if (winningStats.hasOwnProperty(year)){
            if (winningStats[year].hasOwnProperty(team)){
                winningStats[year][team]++;
            }
            else{
                winningStats[year][team] = 1;
            }
        }
        else{
            winningStats[year] = {};
            winningStats[year][team] = 1;
        }
    }

    return winningStats;    
}


// Parsing CSV data for utilizing raw data to get required statistics.
function matchesWonPerTeamPerYear(){
    const matchesData = [];

    fs.createReadStream('src/data/matches.csv')
    .pipe(csv())
    .on('data', (data) => matchesData.push(data))
    .on('end', () => {
        let output = matchesWonPerTeamEveryYear(matchesData);  

        fs.writeFile("src/public/output/2-matches-won-per-team-per-year.json",JSON.stringify(output), function (error){
            if (error){
                console.log(error);
            }
            else{
                console.log("File written successfully");
                console.log("\nThe file writtened has the following data:");
                console.log(fs.readFileSync("src/public/output/2-matches-won-per-team-per-year.json", "utf8"));
            }
        })

    });

}

matchesWonPerTeamPerYear();




