const csv = require('csv-parser')
const fs = require('fs')


// return an array with id's of matches held in 2015
function getMatchIds(matchesData){
    const yearForIdFilter = 2015;
    let idsArray = [];
    let id;
    for (let match = 0; match < matchesData.length; match++){
        id = matchesData[match].id;
        if (matchesData[match].season == yearForIdFilter){
            idsArray.push(id);
        }
    }
    return idsArray;
}


function EconomicalBowlersRanking(idArray,deliveriesData){
// By reduce method, selected all deliveries data for year 2015
    let bowlerRecord,delivery;
    let bowlersDeliveries = deliveriesData.reduce((bowlerRecord,delivery) => {
        if (idArray.includes(delivery.match_id)){
            if (bowlerRecord[delivery.bowler]){
                bowlerRecord[delivery.bowler].runs += parseInt(delivery.total_runs);
                bowlerRecord[delivery.bowler].balls++;
            }
            else{
                bowlerRecord[delivery.bowler] = {runs: parseInt(delivery.total_runs),balls: 1};
            }
        }
        return bowlerRecord;
    },{});

    // Store object with bowler and his economical rate in an array
    let economicalRecord = [];
    let overs,overFractions,bowler,playerEconomicRate;
    for( let index in bowlersDeliveries){
        bowler = bowlersDeliveries[index];
        overFractions = (bowler.balls%6);
        overs = (overFractions == 0) ? (bowler.balls):( ((bowler.balls - overFractions)+ (overFractions/6) )  ) ;
        economicalRecord.push({ [index] : (  parseFloat( (bowler.runs/(overs/6)).toFixed(2) ) ) });
    }
    // Sort array of object having economical rate of bowlers in ascending order
    economicalRecord.sort( (firstBowler,secondBowler) =>{
        let rate1 = Object.values(secondBowler)[0]
        let rate2 = Object.values(firstBowler)[0]
        if (  rate1 > rate2){
            return -1;
        };   
    });
    
    // Convert array of objects to single object
    let accumulator ;
    let finalObjectOfBowlers =  economicalRecord.reduce((accumulator,delivery) => {
        if (economicalRecord.indexOf(delivery) < 10){
        accumulator[Object.keys(delivery)[0]] = Object.values(delivery)[0];
        }
        return accumulator;
    },{});

    // return top 10 economical record
    return finalObjectOfBowlers;
}




function top10EconomicalBowler(){

    const matchesFilePath = "src/data/matches.csv"
    const deliveriesFilePath = "src/data/deliveries.csv"

    const matchesData = [];
    const deliveriesData = [];

    // Parsing CSV data for utilizing raw data to get required statistics.
    fs.createReadStream(matchesFilePath)
    .pipe(csv())
    .on('data', (data) => matchesData.push(data))
    .on('end', () => {
        const idArray = getMatchIds(matchesData);

            fs.createReadStream(deliveriesFilePath)
        .pipe(csv())
        .on('data', (data) => deliveriesData.push(data))
        .on('end', () => {
            const output = EconomicalBowlersRanking(idArray,deliveriesData);

            fs.writeFile("src/public/output/4-top-10-economical-bowlers.json",JSON.stringify(output), function (error){
                if (error){
                    console.log(error);
                }
                else{
                    console.log("File written successfully");
                    console.log("\nThe file writtened has the following data:");
                    console.log(fs.readFileSync("src/public/output/4-top-10-economical-bowlers.json", "utf8"));
                }
            })

        });

    });

}

top10EconomicalBowler();