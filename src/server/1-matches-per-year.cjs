const csv = require('csv-parser')
const fs = require('fs')


// Returns an Object of total matches played for all IPL seasons
function numberOfMatchPerYear(matchesData){
    let years = {};
    for (let match = 0; match <  matchesData.length; match++){
        if (years.hasOwnProperty(matchesData[match].season )){
            years[matchesData[match].season]++  ;
        }
        else{
            years[matchesData[match].season] = 1;
        }
    }
    return years;
}

// Parsing CSV data for utilizing raw data to get required statistics.
function matchesPerYear(){
    
    const matchesData = [];

    fs.createReadStream('src/data/matches.csv')
    .pipe(csv())
    .on('data', (data) => matchesData.push(data))
    .on('end', () => {
        let output = numberOfMatchPerYear(matchesData);  

        fs.writeFile("src/public/output/1-matches-per-year.json",JSON.stringify(output), function (error){
            if (error){
                console.log(error);
            }
            else{
                console.log("File written successfully");
                console.log("\nThe file writtened has the following data:");
                console.log(fs.readFileSync("src/public/output/1-matches-per-year.json", "utf8"));
            }
        })
    
    });

}

matchesPerYear();








