const csv = require('csv-parser')
const fs = require('fs');
const { totalmem } = require('os');

function getMatchIds(matchesData){
    let idsObject = {};
    let id;
    let year;
    for (let match = 0; match < matchesData.length; match++){
        year = matchesData[match].season;
        id = matchesData[match].id;
        idsObject[id] = year;
    }
    return idsObject;
}

function AllBatsmanStrikeRate(idList,deliveriesData){
    let accumulator, delivery;
    let id, batsman, batsmanRuns;

    let batsmanRunsAndBalls = deliveriesData.reduce((batsmanData,delivery) =>{
        id = delivery.match_id;
        batsman = delivery.batsman;
        batsmanRuns = parseInt(delivery.total_runs);

        if( batsmanData.hasOwnProperty(batsman) ){
            if (batsmanData[batsman][idList[id]]){
                batsmanData[batsman][idList[id]].runs += batsmanRuns;
                batsmanData[batsman][idList[id]].balls++;
            }
            else{
                batsmanData[batsman][idList[id]] = {runs : batsmanRuns, balls : 1};  
            }
        }
        else{                                         
            batsmanData[batsman] = {};
            if (idList.hasOwnProperty(id)){
                batsmanData[batsman][idList[id]] = {runs : batsmanRuns, balls : 1};  
            }
        }

        return batsmanData;
    },{});


    // console.log(batsmanRunsAndBalls);
    let batsmanStrikeRate = {};
    let strikeRateFration;
    for( let batsman of Object.keys(batsmanRunsAndBalls)){
        batsmanStrikeRate[batsman] = {};
        for (let year of Object.keys(batsmanRunsAndBalls[batsman])){
            strikeRateFration = (100*(batsmanRunsAndBalls[batsman][year].runs/batsmanRunsAndBalls[batsman][year].balls));
            batsmanStrikeRate[batsman][year] = parseFloat((strikeRateFration).toFixed(2));
        }
    }

    return batsmanStrikeRate;

}



function strikeRateofAllBatsmanForAllYears(){

    const matchesFilePath = "src/data/matches.csv"
    const deliveriesFilePath = "src/data/deliveries.csv"

    const matchesData = [];
    const deliveriesData = [];

    // Parsing CSV data for utilizing raw data to get required statistics.

        fs.createReadStream(matchesFilePath)
        .pipe(csv())
        .on('data', (data) => matchesData.push(data))
        .on('end', () => {
            const idList = getMatchIds(matchesData);

            fs.createReadStream(deliveriesFilePath)
            .pipe(csv())
            .on('data', (data) => deliveriesData.push(data))
            .on('end', () => {
                const output = AllBatsmanStrikeRate(idList,deliveriesData)

            fs.writeFile("src/public/output/7-strike-rate-of-batsman-for-all-seasons.json",JSON.stringify(output), function (error){
                if (error){
                    console.log(error);
                }
                else{
                    console.log("File written successfully");
                    console.log("\nThe file writtened has the following data:");
                    console.log(fs.readFileSync("src/public/output/7-strike-rate-of-batsman-for-all-seasons.json", "utf8"));
                }
            })

        });

    });
}

strikeRateofAllBatsmanForAllYears();