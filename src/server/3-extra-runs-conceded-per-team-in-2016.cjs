const csv = require('csv-parser')
const fs = require('fs')




// return an array with id's of matches held in 2016
function getMatchIds(matchesData){
    const yearForIdFilter = 2016;
    let idsArray = [];
    let id;
    for (let match = 0; match < matchesData.length; match++){
        id = matchesData[match].id;
        if (matchesData[match].season == yearForIdFilter){
            idsArray.push(id);
        }
    }
    return idsArray;
}


function extraConcededRunsForEveryTeam(idArray,deliveriesData){
    let filteredDeliveries = {};
    let team,matchId,extraRun;

    for(let index = 0; index < deliveriesData.length; index++){
        matchId = deliveriesData[index].match_id;
        team = deliveriesData[index].bowling_team;
        extraRun = parseInt(deliveriesData[index].extra_runs);

        if (idArray.includes(matchId)){
            if (filteredDeliveries[team]){
                filteredDeliveries[team] +=  extraRun;
            }
            else{
                filteredDeliveries[team] = extraRun;
            }
        }
    
    }

    return filteredDeliveries;
}


function extraConcededRunsPerTeamIn2016(){

    const matchesFilePath = "src/data/matches.csv"
    const deliveriesFilePath = "src/data/deliveries.csv"
    
    const matchesData = [];
    const deliveriesData = [];
    
    // Parsing CSV data for utilizing raw data to get required statistics.
    fs.createReadStream(matchesFilePath)
    .pipe(csv())
    .on('data', (data) => matchesData.push(data))
    .on('end', () => {
        const idArray = getMatchIds(matchesData);

            fs.createReadStream(deliveriesFilePath)
        .pipe(csv())
        .on('data', (data) => deliveriesData.push(data))
        .on('end', () => {
            const output = extraConcededRunsForEveryTeam(idArray,deliveriesData);
        
            fs.writeFile("src/public/output/3-extra-runs-conceded-per-team-in-2016.json",JSON.stringify(output), function (error){
                if (error){
                    console.log(error);
                }
                else{
                    console.log("File written successfully");
                    console.log("\nThe file writtened has the following data:");
                    console.log(fs.readFileSync("src/public/output/3-extra-runs-conceded-per-team-in-2016.json", "utf8"));
                }
            })
        
        });

    });

}

extraConcededRunsPerTeamIn2016();