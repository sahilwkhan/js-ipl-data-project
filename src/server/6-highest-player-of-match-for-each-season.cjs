const csv = require('csv-parser')
const fs = require('fs')


function highestPlayerOfTheMatchPerYear(matchesData){

    let playerList;
    let match;
    let player;

    // Returns an object of count for every player-of-match for each season
    let playerOfTheMatchList = matchesData.reduce((playerList, match) =>{
        player = match.player_of_match;
        if (playerList.hasOwnProperty(match.season)){
            if (playerList[match.season].hasOwnProperty(player) ){
                    playerList[match.season][player]++;
                }
            else{
                if (player.length > 1){
                    playerList[match.season][player] = 1;
                }
            }
        }   
        else{
            playerList[match.season] = { player : 1};
        }  

        return playerList;  
    },{});

    
    // Returns an object of highest player-of-match count holder for each season
    let highestPlayerObject = {};
    for(let season in playerOfTheMatchList){
        highestPlayerObject[season] = [];
        let highestAwardPlayerForSeason = 0;

        for (let player in playerOfTheMatchList[season]){
            if (playerOfTheMatchList[season][player] > highestAwardPlayerForSeason){
                highestPlayerObject[season] = { [player] : playerOfTheMatchList[season][player] };
                highestAwardPlayerForSeason = playerOfTheMatchList[season][player];
            }
        }
    }

    return highestPlayerObject;

}



function playerWithHighestAwardsPerSeason(){

    const matchesFilePath = "src/data/matches.csv"

    const matchesData = [];

    // Parsing CSV data for utilizing raw data to get required statistics.

        fs.createReadStream(matchesFilePath)
        .pipe(csv())
        .on('data', (data) => matchesData.push(data))
        .on('end', () => {
            const output = highestPlayerOfTheMatchPerYear(matchesData);

            fs.writeFile("src/public/output/6-highest-player-of-match-for-each-season.json",JSON.stringify(output), function (error){
                if (error){
                    console.log(error);
                }
                else{
                    console.log("File written successfully");
                    console.log("\nThe file writtened has the following data:");
                    console.log(fs.readFileSync("src/public/output/6-highest-player-of-match-for-each-season.json", "utf8"));
                }
            })

        });

}

playerWithHighestAwardsPerSeason();