// 1. Chart for matches held per year

fetch("./output/1-matches-per-year.json")
    .then((data) => data.json())
    .then((data) => {
        console.log("First Function data loaded", data);


        let yearData, year;
        let yearList = Object.entries(data).reduce((yearData, year) => {
            yearData.push(year[1]);
            return yearData;
        }, [ ])

        console.log(yearList);
 
        // Set up the chart
    
        Highcharts.chart('firstFunction', {

            title: {
                text: '',
                align: 'left'
            },

            // subtitle: {
            //     text: 'Source: <a href="https://irecusa.org/programs/solar-jobs-census/" target="_blank">IREC</a>',
            //     align: 'left'
            // },

            yAxis: {
                title: {
                    text: 'Number of Matches'
                }
            },

            xAxis: {
                accessibility: {
                    rangeDescription: 'IPL Seasons'
                }
            },

            // legend: {
            //     layout: 'bottom',
            //     align: 'bottom',
            //     verticalAlign: 'middle'
            // },

            plotOptions: {
                series: {
                    label: {
                        connectorAllowed: false
                    },
                    pointStart: 2008
                }
            },

            series: [{name : "Number of Matches", data : yearList}],

            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom'
                        }
                    }
                }]
            }

        });

});


// 2. Chart for matches won by teams per year

fetch("./output/2-matches-won-per-team-per-year.json")
.then((data) => data.json())
.then((data) => {
  const matchesWonByTeamsPerYear = Object.values(data);
  console.log("Second Function data loaded",matchesWonByTeamsPerYear);
  
  let teamObject, teamWithData;
    let matchWinTeamsData = matchesWonByTeamsPerYear.reduce((teamObject, seasonData) => {
        let teamsData;
        Object.keys(seasonData).map((teamData) => {
            if (teamObject[teamData]) {
                teamObject[teamData].push(seasonData[teamData]);
            }
            else {
                teamObject[teamData] = [seasonData[teamData]]
            }
        });
        return teamObject;
    }, {})
        
    let matchWinData = Object.keys(matchWinTeamsData).map((teamWithData) => {
        return {name : teamWithData, data : matchWinTeamsData[teamWithData]}
     });
 
    // console.log(matchWinData);

  
  let matchWinArray,teamData;
    let matchWinDataForChart = Object.keys(matchWinData).reduce((matchWinArray, teamName) => {
      
  },[]);
 
  
  Highcharts.chart('secondFunction', {
    chart: {
        type: 'area'
    },
    title: {
        text: '',
        align: 'left'
    },
    // subtitle: {
    //     text: 'Source: ' +
    //         '<a href="https://www.ssb.no/en/statbank/table/09288/"' +
    //         'target="_blank">SSB</a>',
    //     align: 'left'
    // },
    yAxis: {
        title: {
            useHTML: true,
            text: ''
        }
    },
    tooltip: {
        shared: true,
        headerFormat: '<span style="font-size:12px"><b>{point.key}</b></span><br>'
    },
    plotOptions: {
        series: {
            pointStart: 2008
        },
        area: {
            stacking: 'normal',
            lineColor: '#666666',
            lineWidth: 1,
            marker: {
                lineWidth: 1,
                lineColor: '#666666'
            }
        }
    },
    series: matchWinData
});

});




// 3. Chart for extra runs conceded per team in 2016

fetch("./output/3-extra-runs-conceded-per-team-in-2016.json")
.then((data) => data.json())
.then((data) => {
  const extraRunsGivenByEachTeam = data
  console.log("Third Function data loaded",extraRunsGivenByEachTeam);

  let extraRunsList, teamConcedingRuns;
  let extraRunsArray = Object.values(extraRunsGivenByEachTeam).reduce( (extraRunsList, teamConcededRuns) =>{
    if(extraRunsList[0].data){
      extraRunsList[0].data.push(teamConcededRuns);
    }
    return extraRunsList;
  },[{ data : [], colorByPoint : true} ])


  let teamNamesForExtraRunsList;
  let teamsList = Object.keys(extraRunsGivenByEachTeam);
 
  // Set up the chart
  
const chart = new Highcharts.Chart({
  chart: {
      renderTo: 'thirdFunction',
      type: 'column',
      options3d: {
          enabled: true,
          alpha: 20,
          beta: 20,
          depth: 0,
          viewDistance: 25
      }
  },
  xAxis: {
      categories: teamsList
  },
  yAxis: {
      title: {
          enabled: false
      }
  },
  tooltip: {
      headerFormat: '<b>{point.key}</b><br>',
      pointFormat: 'Extra Runs Conceded: {point.y}'
  },
  title: {
      text: '',
      align: 'left'
  },
  // subtitle: {
  //     text: 'Source: ' +
  //         '<a href="https://gitlab.com/sahilwkhan/js-ipl-data-project/-/blob/master/src/public/output/3-extra-runs-conceded-per-team-in-2016.json"' +
  //         'target="_blank">IPL</a>',
  //     align: 'left'
  // },
  legend: {
      enabled: false
  },
  plotOptions: {
      column: {
          depth: 25
      }
  },
  series: extraRunsArray,

});

});


// 4. Top 10 economical bowlers in the year 2015

fetch("./output/4-top-10-economical-bowlers.json")
.then((data) => data.json())
.then((data) => {
  const top2015BowlerData = data;
  console.log("Fourth Function data loaded",top2015BowlerData);


    let bowlerNames = Object.keys(top2015BowlerData).reduce((array, pair) => {
        array.push( [pair,top2015BowlerData[pair],1]);
        return array;
    },[]);
 
  // Set up the chart
  
  Highcharts.chart('fourthFunction', {

    chart: {
        type: 'variwide'
    },

    title: {
        text: ''
    },

    // subtitle: {
    //     text: 'Source: <a href="http://ec.europa.eu/eurostat/web/' +
    //         'labour-market/labour-costs/main-tables">eurostat</a>'
    // },

    xAxis: {
        type: 'category'
    },

    legend: {
        enabled: false
    },

    series: [{
        name: 'Labor Costs',
        data: bowlerNames,
        dataLabels: {
            enabled: true,
            format: ''
        },
        tooltip: {
            pointFormat: 'Strike Rate : <b>{point.y}</b><br>' },
        colorByPoint: true
    }]

});

  
});

// 5. Team that won the toss and match

fetch("./output/5-teams-that-won-toss-and-match.json")
.then((data) => data.json())
.then((data) => {
  const teamsThatWonTossAndMatch = data;
  console.log("Fifth Function data loaded",teamsThatWonTossAndMatch);

  let teamslist, team;
  let tossAndMatchWonTeamsData = Object.keys(teamsThatWonTossAndMatch).reduce( (teamslist, team) =>{
      teamslist.push([team, teamsThatWonTossAndMatch[team]]);
      return teamslist;
  }, [])
    
    // console.log(tossAndMatchWonTeamsData);
 
  // Set up the chart
  
  Highcharts.chart('fifthFunction', {
    chart: {
        type: 'pie',
        options3d: {
            enabled: true,
            alpha: 45,
            beta: 0
        }
    },
    title: {
        text: '',
        align: 'left'
    },
    // subtitle: {
    //     text: 'Source: ' +
    //         '<a href="https://www.counterpointresearch.com/global-smartphone-share/"' +
    //         'target="_blank">Counterpoint Research</a>',
    //     align: 'left'
    // },
    accessibility: {
        point: {
            valueSuffix: '%'
        }
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.y}</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            depth: 35,
            dataLabels: {
                enabled: true,
                format: '{point.name}'
            }
        }
    },
    series: [{
        type: 'pie',
        name: 'Count of winning toss and match',
        data: tossAndMatchWonTeamsData
    }]
});

  
});




// 6. Players who has won the highest number of Player of the Match awards for each season

fetch("./output/6-highest-player-of-match-for-each-season.json")
.then((data) => data.json())
    .then((data) => {
        
        const highestPlayerofMAtchAwardsPerSeason = data;
        console.log("Sixth Function data loaded", highestPlayerofMAtchAwardsPerSeason);

        let yearForHighestAward = Object.keys(highestPlayerofMAtchAwardsPerSeason);
        // console.log(yearForHighestAward);

        let nameAndRuns, player;
        let nameOfHighestAwardPlayer = Object.keys(highestPlayerofMAtchAwardsPerSeason).reduce((nameAndRuns, awardYear) => {
            let nameAndRunsObject = { name: Object.keys(highestPlayerofMAtchAwardsPerSeason[awardYear])[0], data: [] };
            let yearListData;
            yearForHighestAward.map((yearListData) => {
                if ( awardYear  == yearListData) {
                    nameAndRunsObject.data.push(Object.values(highestPlayerofMAtchAwardsPerSeason[awardYear])[0]);
                } 
                else{
                    nameAndRunsObject.data.push(0);
                }
                return nameAndRunsObject;
            });
            nameAndRuns.push(nameAndRunsObject);
            return nameAndRuns;
        }, [])
        
        console.log(nameOfHighestAwardPlayer);

 
  // Set up the chart
    
Highcharts.chart('sixthFunction', {
    chart: {
        type: 'bar'
    },
    title: {
        text: "" 
    },
    xAxis: {
        categories: yearForHighestAward
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Number of Player of Match Won'
        }
    },
    legend: {
        reversed: true
    },
    plotOptions: {
        series: {
            stacking: 'normal',
            dataLabels: {
                enabled: true
            }
        }
    },
    series: nameOfHighestAwardPlayer,
});

});

// 7. Strike rate of a batsman for each season

fetch("./output/7-strike-rate-of-batsman-for-all-seasons.json")
.then((data) => data.json())
.then((data) => {
    let strikeRateInfo = data;
    console.log("Seventh Function data loaded",strikeRateInfo);

    let firstObject = Object.keys(strikeRateInfo)[0];
    let yearList, year 
    let yearArray = Object.values(strikeRateInfo).reduce((yearList, oneObject) => {
        let year;
        Object.keys(oneObject).map((year) => {
            if (!yearList.includes(year)) {
                yearList.push(year)
            }
        })
    return yearList.sort()
    },[])
    //   console.log(yearArray);
    

    let batsmanData, batsmanObject,playedYear;
    let batsmanStrikeData = Object.keys(strikeRateInfo).reduce((batsmanData, batsmanObject) => {
    
        let pushObject = { name: batsmanObject, data: [] };
     
        Object.values(yearArray).map((playedYear) => {
            if (Object.keys(strikeRateInfo[batsmanObject]).includes(playedYear)) {
                pushObject.data.push(strikeRateInfo[batsmanObject][playedYear]);
            }
            else{
                pushObject.data.push(0);
            }
        });
    
        batsmanData.push(pushObject);
        return batsmanData;
    }, []);

// console.log(batsmanStrikeData);
 
  // Set up the chart
  // Data retrieved from: https://www.uefa.com/uefachampionsleague/history/
Highcharts.chart('seventhFunction', {
    chart: {
        type: 'bar'
    },
    title: {
        text: ''
    },
    xAxis: {
        categories: yearArray
    },
    yAxis: {
        min: 0,
        title: {
            text: ''
        }
    },
    legend: {
        reversed: true
    },
    plotOptions: {
        series: {
            stacking: 'normal',
            dataLabels: {
                enabled: true
            }
        }
    },
    series: batsmanStrikeData

});

});

// 8. Highestt times one player dismissed another


fetch("./output/8-highest-times-one-player-dismissed-another.json")
.then((data) => data.json())
    .then((data) => {
        
        const highestTimesDismissedByBowler = data;
        console.log("Eigth Function data loaded", highestTimesDismissedByBowler);

        let bowlerWithHighestDismissal = Object.values(highestTimesDismissedByBowler);

 
  // Set up the chart
    
Highcharts.chart('eightFunction', {
    chart: {
        type: 'bar'
    },
    title: {
        text: "" 
    },
    xAxis: {
        categories: [bowlerWithHighestDismissal[0]],
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Number of times dismissed'
        }
    },
    legend: {
        reversed: true
    },
    plotOptions: {
        series: {
            stacking: 'normal',
            dataLabels: {
                enabled: true
            }
        }
    },
    series: [ {name : bowlerWithHighestDismissal[1], data : [bowlerWithHighestDismissal[2]] } ],
});

});


// 9. bowler with best economical rate in super over

fetch("./output/9-bowler-with-best-economy-in-super-overs.json")
.then((data) => data.json())
    .then((data) => {
        
        const bestSuperOverBowler = data;
        console.log("Ninth Function data loaded", bestSuperOverBowler);

 
  // Set up the chart
    
Highcharts.chart('ninthFunction', {
    chart: {
        type: 'bar'
    },
    title: {
        text: "" 
    },
    xAxis: {
        categories: Object.keys(bestSuperOverBowler),
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Economical rate of bowler',
        }
    },
    legend: {
        reversed: true
    },
    plotOptions: {
        series: {
            stacking: 'normal',
            dataLabels: {
                enabled: true
            }
        }
    },
    series: [{ name : "Economy" ,  data : Object.values(bestSuperOverBowler) }],
});

});
